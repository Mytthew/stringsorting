package com.company;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> firstList = Arrays.asList("Jeden", "Dwa", "Trzy", "Cztery");
        List<String> secondList = Arrays.asList("Jeden", "Dwa", "Trzy", "Cztery");
        List<String> thirdList = Arrays.asList("Jeden", "Dwa", "Trzy", "Cztery");
        System.out.println("First list = " + firstList);
        bubbleSort(firstList);
        System.out.println("New first list = " + firstList + "\n");
        System.out.println("Second list = " + secondList);
        selectionSort(secondList);
        System.out.println("New second list = " + secondList + "\n");
        System.out.println("Third list = " + thirdList);
        insertionSort(thirdList);
        System.out.println("New third list = " + thirdList);
    }

    public static void bubbleSort(List<String> myList) {
        for (int i = 0; i < myList.size() ; i++) {
            for (int j = 0; j < myList.size() - 1; j++) {
                if ((myList.get(j)).compareTo(myList.get(j + 1)) > 0) {
                    String temp = myList.get(j);
                    myList.set(j, myList.get(j + 1));
                    myList.set(j + 1, temp);
                }
            }
        }
    }

    public static void selectionSort(List<String> myList) {
        for (int j = 0; j < myList.size(); j++) {
            int temp = j;
            for (int i = j; i < myList.size(); i++) {
                if ((myList.get(i).compareTo(myList.get(temp))) < 0) {
                    temp = i;
                }
            }
            String temp2 = myList.get(j);
            myList.set(j, myList.get(temp));
            myList.set(temp, temp2);
        }
    }

    public static void insertionSort(List<String> myList) {
        for (int i = 1; i < myList.size(); i++) {
            for (int j = i; j > 0; j--) {
                if (myList.get(j - 1).compareTo(myList.get(j)) > 0) {
                    String temp = myList.get(j);
                    myList.set(j, myList.get(j - 1));
                    myList.set(j - 1, temp);
                }
            }
        }
    }
}

